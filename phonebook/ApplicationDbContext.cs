﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;
using phonebook.Models;
using System.Text.RegularExpressions;

public class ApplicationDbContext : DbContext
{
    public DbSet<Country> Countries { get; set; }
    public DbSet<User> Users { get; set; }



    public ApplicationDbContext(DbContextOptions options)
        : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder builder)
    {
        base.OnModelCreating(builder);
    }
}