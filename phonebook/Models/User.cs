﻿using CsvHelper.Configuration.Attributes;
using System.ComponentModel.DataAnnotations;

namespace phonebook.Models
{
    public class User
    {
        public int Id { get; set; }
        public string Name { get; set; }
        [MaxLength(10)]
        public string PhoneNumber { get; set; }

        public int CountryId { get; set; }
        public virtual Country? Country { get; set; }
    }
}
