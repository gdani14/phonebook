﻿using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations;
using System.Security.Policy;

namespace phonebook.Models
{
    [Index(nameof(CallingCode))]
    public class Country
    {
        public int Id { get; set; }
        public string Name { get; set; }
        [MaxLength(2)]
        public string Code { get; set; }
        [MaxLength(7)]
        public string CallingCode { get; set; }
    }
}
