﻿using CsvHelper.Configuration.Attributes;

namespace phonebook.Models.DTOs
{
    public class CountryDTO
    {
        [Ignore]
        public int Id { get; set; }
        [Index(0)]
        public string Name { get; set; }
        [Index(1)]
        public string Code { get; set; }
        [Index(2)]
        public string CallingCode { get; set; }
    }
}
