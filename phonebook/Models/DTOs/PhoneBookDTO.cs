﻿using CsvHelper.Configuration.Attributes;

namespace phonebook.Models.DTOs
{
    public class PhoneBookDTO
    {
        [Index(0)]
        public string Name { get; set; }
        [Index(1)]
        public string PhoneNumber { get; set; }
    }
}
