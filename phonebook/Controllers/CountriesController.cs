﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CsvHelper.Configuration;
using CsvHelper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using phonebook.Models;
using System.Globalization;
using phonebook.Models.DTOs;
using System.Text.RegularExpressions;

namespace phonebook.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CountriesController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        CsvConfiguration csvConfiguration = new CsvConfiguration(CultureInfo.InvariantCulture)
        {
            HasHeaderRecord = false,
            Delimiter = ";",
        };

        public CountriesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/Countries
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Country>>> GetCountries()
        {
            return await _context.Countries.ToListAsync();
        }

        // GET: api/Countries/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Country>> GetCountry(int id)
        {
            var country = await _context.Countries.FindAsync(id);

            if (country == null)
            {
                return NotFound();
            }

            return country;
        }

        // POST: api/Countries
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [DisableRequestSizeLimit]
        [HttpPost]
        public async Task<ActionResult<Country>> PostCountry([FromForm] IFormFile inputFile)
        {
            if (inputFile == null || inputFile.Length == 0) BadRequest("File has no contents!");
            using (FileStream fs = System.IO.File.Create(inputFile.FileName))
            {
                inputFile.CopyTo(fs);
            }

            if (_context.Countries.Any()) _context.RemoveRange(_context.Countries);
            

            List<CountryDTO> countryDTOs;
            using (var reader = new StreamReader(inputFile.FileName))
            using (var csv = new CsvReader(reader, csvConfiguration))
            {
                try
                {
                    countryDTOs = csv.GetRecords<CountryDTO>().ToList();
                }
                catch (Exception e)
                {
                    return Problem("Import error!");
                }
            }


            string callingCodePattern = @"^\+([0-9]\-)?[0-9]{1,5}$";
            string codePattern = @"^[a-z]{1,2}$";
            foreach (var input in countryDTOs)
            {
                if (Regex.IsMatch(input.CallingCode, callingCodePattern, RegexOptions.IgnoreCase) && Regex.IsMatch(input.Code, codePattern, RegexOptions.IgnoreCase))
                {
                    Country country = new Country();
                    country.Name = input.Name;
                    country.Code = input.Code;
                    country.CallingCode = input.CallingCode.Substring(1);
                    _context.Countries.Add(country);
                }
            }

            await _context.SaveChangesAsync();
            System.IO.File.Delete(inputFile.FileName);
            return StatusCode(201);
        }

        private bool CountryExists(int id)
        {
            return _context.Countries.Any(e => e.Id == id);
        }
    }
}
