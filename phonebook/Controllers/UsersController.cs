﻿using System;
using System.Collections.Generic;
using System.Formats.Asn1;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using CsvHelper;
using CsvHelper.Configuration;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient.Server;
using Microsoft.EntityFrameworkCore;
using phonebook.Models;
using phonebook.Models.DTOs;
using Z.BulkOperations;

namespace phonebook.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        BulkOperation bulk = new BulkOperation();
        CsvConfiguration csvConfiguration = new CsvConfiguration(CultureInfo.InvariantCulture)
        {
            HasHeaderRecord = false,
            Delimiter = ";",
        };

        public UsersController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/Users
        [HttpGet]
        public async Task<ActionResult<IEnumerable<User>>> GetUsers()
        {
            return await _context.Users.Take(1000).ToListAsync();
        }

        // GET: api/Users/find
        [HttpGet("find")]
        public async Task<ActionResult<IEnumerable<User>>> GetUsers(string name)
        {
            name = name.ToLower().Trim();
            if (name == "")
            {
                return await GetUsers();
            }

            return await _context.Users.Where(u => u.Name.ToLower().Contains(name)).ToListAsync();
        }

        // GET: api/Users/5
        [HttpGet("{id}")]
        public async Task<ActionResult<User>> GetUser(int id)
        {
            var user = await _context.Users.FindAsync(id);

            if (user == null)
            {
                return NotFound();
            }

            return user;
        }

        // PUT: api/Users/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutUser(int id, User user)
        {
            if (id != user.Id)
            {
                return BadRequest();
            }

            _context.Entry(user).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Users
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<User>> PostUser(User user)
        {
            User? existingUser;
            if(user.Id == 0)
            {
                existingUser = null;
            } 
            else
            {
                existingUser = _context.Users.Where(u => u.Id.Equals(user.Id)).FirstOrDefault();
            }

            if(existingUser == null)
            {
                User newUser = new User();
                newUser.Id = user.Id;
                newUser.PhoneNumber = user.PhoneNumber;
                newUser.Name = user.Name;
                newUser.CountryId = user.CountryId;
                _context.Users.Add(newUser);
            } 
            else
            {
                existingUser.Id = user.Id;
                existingUser.Name = user.Name;
                existingUser.PhoneNumber = user.PhoneNumber;
                existingUser.CountryId = user.CountryId;
            }
            
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetUser", new { id = user.Id }, user);
        }

        // POST: api/Users(file)
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [DisableRequestSizeLimit]
        [HttpPost("file")]
        public async Task<IActionResult> PostUser([FromForm] IFormFile inputFile)
        {
            if (!_context.Countries.Any()) return Problem("There are no Coutries defined in the database! Use /api/Countries [POST]" +
                " for inserting Countries with a CSV file!");
            if (inputFile == null || inputFile.Length == 0) BadRequest("File has no contents!");

            using (FileStream fs = System.IO.File.Create(inputFile.FileName))
            {
                inputFile.CopyTo(fs);
            }

            List<PhoneBookDTO> phoneBookDTOs;
            using (var reader = new StreamReader(inputFile.FileName))
            using (var csv = new CsvReader(reader, csvConfiguration))
            {
                try
                {
                    phoneBookDTOs = csv.GetRecords<PhoneBookDTO>().ToList();
                }
                catch(Exception e)
                {
                    return Problem("Import error!");
                }
            }

            List<Country> countries = _context.Countries.ToList();
            List<User> users = new List<User>();

            string phoneNumberPattern = @"^\+([0-9]\-)?[0-9]{1,5}\s[0-9]{7,10}$";
            foreach (var input in phoneBookDTOs)
            {
                if (Regex.IsMatch(input.PhoneNumber, phoneNumberPattern, RegexOptions.IgnoreCase))
                {
                    User user = new User();
                    user.Name = input.Name.Trim();
                    var phoneNumberSplit = input.PhoneNumber.Split(" ");
                    var tempCCC = phoneNumberSplit.FirstOrDefault();
                    if (tempCCC == null) break;
                    else user.PhoneNumber = input.PhoneNumber.Substring(tempCCC.Length + 1).Trim();
                    var tempCountry = countries.Where(c => c.CallingCode.ToLower().Trim() == tempCCC.Substring(1).ToLower().Trim()).FirstOrDefault();
                    if (tempCountry == null) break;
                    else user.CountryId = tempCountry.Id;

                    //_context.Users.Add(user);
                    users.Add(user);
                }
            }

            _context.Users.AddRange(users);
            if (users.Count > 1000) _context.BulkSaveChanges(options => options.BatchSize = 1000);
            else await _context.SaveChangesAsync();
            return StatusCode(201);
        }

        // DELETE: api/Users/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteUser(int id)
        {
            var user = await _context.Users.FindAsync(id);
            if (user == null)
            {
                return NotFound();
            }

            _context.Users.Remove(user);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool UserExists(int id)
        {
            return _context.Users.Any(e => e.Id == id);
        }
    }
}
